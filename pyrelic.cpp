#include <boost/python.hpp>

#include "relicwrapper.h"

using namespace boost::python;

// https://www.auctoris.co.uk/2017/12/21/advanced-c-python-integration-with-boost-python-part-2/
PyObject* G1_class_toString_wrap(G1& self, object base)
{
    PyObject* pymemview;
    std::string export_data;

    export_data = self.toString(extract<unsigned short>(base));

    pymemview = PyMemoryView_FromMemory((char*) export_data.c_str(), export_data.length(), PyBUF_READ);
    return PyBytes_FromObject(pymemview);
}

void Zr_class_fromBin_wrapper(Zr& self, object input)
{
    stl_input_iterator<int> begin(input), end;
    std::vector<char> buffer(begin, end);
    self.fromBin(reinterpret_cast<const char*>(&buffer[0]));
}


BOOST_PYTHON_MODULE(pyrelic)
{
    class_<Zr>("Zr",init<>())
      .def(init<long long int>())
	  .def(init<Pairing,long long int>()) // init from Pairing!
      .def("toString",&Zr::toString)
      .def("__str__",&Zr::decimalToString)
	  .def("__repr__",&Zr::decimalToString)
      .def("fromString",&Zr::fromString)
	  //.def("fromBin",&Zr::fromBin)
    .def("fromBin",&Zr_class_fromBin_wrapper)
      .def("randomize",&Zr::randomize)
      .def("__add__",&Zr::operator+)
      .def("__sub__",&Zr::operator-)
      .def("__mul__",&Zr::operator*)
      //.def("__div__",&Zr::operator/)   // Python 2
      .def("__truediv__",&Zr::operator/)  // Python 3
	  .def("__mod__",&Zr::operator%)
      .def("__pow__",&Zr::operator^)
      .def("__xor__",&Zr::operator^) //__xor__ = ^ symbol, __pow__ = **; I've never liked this aspect of Python, so why not correct it
      .def("__eq__",&Zr::operator==)
	  .def("__ne__",&Zr::operator!=)
	  .def("cmp",&Zr::cmp)
	  .def("mod2m",&Zr::mod2m)
	  .def("div",&Zr::div)
    ;
    class_<G1>("G1",init<>())
      .def(init<bool>())
	  .def(init<Pairing,bool>())
      //.def("toString",&G1::toString)
      .def("toString", &G1_class_toString_wrap)
      /*.def("toString",
        [](const G1 &a){
          std::string s = a.toString(); //&G1::toString
          //std::string s("\xba\xd0\xba\xd0");  // May be not valid UTF-8
          return py::bytes(s);  // Return the data without transcoding
        }
      )*/
      .def("__str__",&G1::decimalToString)
	  .def("__repr__",&G1::decimalToString)
      .def("fromString",&G1::fromString)
      .def("randomize",&G1::randomize)
      .def("__mul__",&G1::operator*)
      //.def("__div__",&G1::operator/)
      .def("__truediv__",&G1::operator/)
      .def("__pow__",&G1::operator^)
      .def("__xor__",&G1::operator^)
      .def("__eq__",&G1::operator==)
	  .def("__ne__",&G1::operator!=)
    ;
    class_<G2>("G2",init<>())
      .def(init<bool>())
      .def("toString",&G2::toString)
      .def("__str__",&G2::decimalToString)
      .def("fromString",&G2::fromString)
      .def("randomize",&G2::randomize)
      .def("__mul__",&G2::operator*)
      .def("__div__",&G2::operator/)
      .def("__pow__",&G2::operator^)
      .def("__xor__",&G2::operator^)
      .def("__eq__",&G2::operator==)
	  .def("__ne__",&G2::operator!=)
    ;
    class_<GT>("GT",init<>())
      .def(init<bool>())
      .def("toString",&GT::toString)
      .def("__str__",&GT::decimalToString)
      .def("fromString",&GT::fromString)
      .def("randomize",&GT::randomize)
      .def("__mul__",&GT::operator*)
      .def("__div__",&GT::operator/)
      .def("__pow__",&GT::operator^)
      .def("__xor__",&GT::operator^)
      .def("__eq__",&GT::operator==)
	  .def("__ne__",&GT::operator!=)
    ;
    class_<Pairing>("Pairing",init<>())
      .def("__call__",&Pairing::operator())
	  .def("get_g1_gen",&Pairing::get_g1_gen)
         .def("get_g2_gen",&Pairing::get_g2_gen)
	  .def("get_ord",&Pairing::get_ord)
    ;
    def("initPairing", initPairing);
    def("delPairing", delPairing);
}
