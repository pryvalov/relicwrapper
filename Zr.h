#ifndef __Zr_H__
#define __Zr_H__

#include <iostream>
#include "G.h"
//#include "Pairing.h"
#include <gmp.h>
//#undef __cplusplus
extern "C" {
#include <relic/relic.h>
}
//#define __cplusplus 1

class Zr {
public:
	Zr();
	virtual ~Zr() { bn_free(g); bn_free(ord);  }

	//Basic initialization to zero
	Zr(const Pairing &e);
  
	//random = true  -> random value
	//random = false -> zero
	//Zr(const Pairing &e, bool random);
	//Zr(bool random) { initMaybeRandom(random); }

	//Copy constructor
	Zr(const Zr& other);

	//Create an element from a long int
	Zr(const Pairing &e, long long int i);
	Zr(const Pairing &e, long int i);
	Zr(const Pairing &e, int i);
	Zr(long long int i) { initWithNum((unsigned long long int) i); }
	Zr(long int i) { initWithNum((unsigned long long int) i); }
	Zr(int i) { initWithNum((unsigned long long int) i); }

	//From hash
	Zr(const Pairing &e, const void *data, unsigned short len);

	//Init from string
	Zr(const std::string &s, unsigned short base = 10);

	//PBC-style import
	Zr(const Pairing &e, const unsigned char *data,
			unsigned short len, unsigned short base = 16);

  //Initialize from another, but with a different value (for compatibility)
//  Zr(const Zr& other, long int i) { initWithNum((unsigned long long int) i); }

	//Arithmetic operators
	Zr& operator+=(const Zr& other);
	Zr& operator*=(const Zr& other);
	Zr& operator-=(const Zr& other);
	Zr& operator/=(const Zr& other);
	Zr& operator%=(const Zr& other);
	Zr& operator^=(const Zr& other);
private:
	Zr& mod2m_(int m);
	Zr& div_(const Zr& other);
public:
	int get_bit(int pos) const;

public:
	const Zr operator+(const Zr &other) const {
		return Zr(*this) += other;
	}
	const Zr operator*(const Zr &other) const {
		return Zr(*this) *= other;
	}
	const Zr operator-(const Zr &other) const {
		return Zr(*this) -= other;
	}
	const Zr operator/(const Zr &other) const {
		return Zr(*this) /= other;
	}
	const Zr operator%(const Zr &other) const {
		return Zr(*this) %= other;
	}
	const Zr operator^(const Zr &other) const {
		return Zr(*this) ^= other;
	}
	const Zr mod2m(int m) const {
		return Zr(*this).mod2m_(m);
	}
	const Zr div(const Zr& other) const {
		return Zr(*this).div_(other);
	}

	int cmp(const Zr& other) const;
	bool operator==(const Zr &other) const;
	bool operator!=(const Zr &other) const { return !(*this==other);}

	const Zr square() {
		Zr a = Zr(0);
		a.setElement(g);
		a.copyOrder(*this);
		return a * a;
	}
	const Zr inverse(bool additive = false) const {
		Zr tempz(0);
		if (additive) {
			tempz = Zr(0);
			tempz.copyOrder(*this);
			tempz -= *this;
		} else {
			tempz = Zr(1);
			tempz.copyOrder(*this);
			tempz /= *this;
		}
		return tempz;
	}
	void operator=(const Zr &other);
	void copyOrder(const Zr& other);

	//String and I/O methods
	virtual std::string toString(unsigned short base = 10) const;
	std::string decimalToString () const { return toString(10); }

	void dump(FILE *f, const char *label = "",
			unsigned short base = 10) const {
		if (base < 256) {
			if (label)
				fprintf(f, "%s: ", label);
			fprintf(f, "%s\n", toString(base).c_str());
		} else {
			char o[1000];
			toBin(o);
			for (int i = 0; i < 32; i++) {
				fputc(o[i], f);
			}
		}
	}

	void fromBin(const char* input);
	virtual void toBin(char* output) const;

	void fromString(const std::string &input, unsigned short base = 10) {
		std::istringstream iss(input);
		fromStream(iss, base);
	}

	friend inline std::ostream& operator<<(std::ostream &out, const Zr& x) {
		out << x.toString(10);
		return out;
	}

	friend inline std::istream& operator>>(std::istream &in, Zr& out) {
		out.fromStream(in, 10);
		return in;
	}

	void fromStream(std::istream &in, int base);

	//Helper functions
	void getElement(bn_t out) const {
		bn_copy(out, const_cast<bn_st*>(g));
	}
	void setElement(bn_t in) {
		bn_copy(g, in);
	}

	void getOrder(bn_t out) const {
		bn_copy(out, const_cast<bn_st*>(ord));
	}
	void setOrder(bn_t in) {
		bn_copy(ord, in);
	}

	bool isIdentity(bool additive) { return *this == Zr(additive ? 0 : 1); }
	bool isElementPresent() const { return true; }
	Zr randomize();
	void setOrder(const Zr &order);

protected:
	bn_t g;
	bn_t ord;
	void init();
	virtual void initWithNum(unsigned long long int i);
};

#endif
