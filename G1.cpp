#include "G1.h"

extern "C" {
#include <relic/relic.h>
}

using namespace std;

void G1::init() { g1_new(g); }

//Basic initialization to point at infinity
G1::G1() {
  init();
  g1_set_infty(g);
}

G1::G1(const Pairing &e) {
  init();
  g1_set_infty(g);
}

//identity = true  -> point at infinity
//identity = false -> random value
G1::G1(const Pairing &e, bool identity) {
  boolInit(identity);
}

G1::G1(bool identity) {
  boolInit(identity);
}

//identity = true  -> point at infinity
//identity = false -> copy constructor
G1::G1(const G1 &h, bool identity) {
  if (identity) {
    boolInit(true);
  }
  else {
    init();
    WITH_TEMP(g1, temp, {
      h.getElement(temp);
      setElement(temp);
    })
  }
}

void G1::boolInit(bool identity) {
  init();
  if (identity) {
    g1_set_infty(g);
  }
  else {
    g1_rand(g);
  }
}

G1 G1::randomize() { g1_rand(g); G1 o; o.setElement(g); return o; }

bool G1::isIdentity() const {
  WITH_TEMP_RETURN(g1, temp, {
    g1_set_infty(temp);
    ans = g1_cmp(const_cast<ep_st*>(g),temp) == 0;
  },bool,ans)
}

//Arithmetic operators
G1& G1::operator*=(const G1& other) {
  WITH_TEMP(g1, temp, {
    other.getElement(temp);
    g1_add(g,g,temp);
    g1_norm(g,g);
  })
  return *this;
}

G1& G1::operator/=(const G1& other) {
  WITH_TEMP(g1, temp, {
    other.getElement(temp);
    g1_sub(g,g,temp);
    g1_norm(g,g);
  })
  return *this;
}

G1& G1::operator^=(const Zr& other) {
	if (*this == G1())
		return *this; //identity case! it seems, there is a bug in relic, which leads to hanging
  WITH_TEMP(bn, temp, {
    other.getElement(temp);
    g1_mul(g,g,temp);
    g1_norm(g,g);
  })
  return *this;
}

bool G1::operator==(const G1& other) const {
  WITH_TEMP_RETURN(g1, temp, {
    other.getElement(temp);
    ans = g1_cmp(const_cast<ep_st*>(g),const_cast<ep_st*>(temp)) == 0;
  },bool,ans)
}

//Assignment operator
void G1::operator=(const G1& other) {
  WITH_TEMP(g1,temp, {
    other.getElement(temp);
    setElement(temp);
  })
}

//Init from hash
G1::G1(const Pairing &e, const void *data, unsigned short len) {
  init();
  unsigned char cdata[len];
  memcpy(cdata,data,len);
  g1_map(g,cdata,len);
}

//Init from string
G1::G1(const string &s, unsigned short base) {
  init();
  fromString(s,base);
}

//Outputs to string, readable with fromString
string G1::toString(unsigned short base) const {
  if (base < 256) {
    char x[257], y[257], z[257];
    ep_st* h = const_cast<ep_st*>(g);
    fp_write_str(x,257,h->x,base);
    fp_write_str(y,257,h->y,base);
    fp_write_str(z,257,h->z,base);
    char o[1000];
    sprintf(o,(base == 10) ? "[%s,%s,%s]" : "[\"%s\",\"%s\",\"%s\"]",x,y,z);
    string outstr(o);
    return outstr;
  }
  else {
    char o[64];
    toBin(o);
    return string(o,64);
  }
}

//Base-256 methods
void G1::fromBin(const char* input) {
    FP_bin_read(g->x,const_cast<char*>(input),32);
    FP_bin_read(g->y,const_cast<char*>(input+32),32);
    char buffer[32];
    for (int i = 0; i < 32; i++) buffer[i] = (i==31) ? '\x01' : '\x00';
    FP_bin_read(g->z,buffer,32);
}
void G1::toBin(char* output) const {
    FP_bin_write(output,32,const_cast<dig_t*>(g->x));
    FP_bin_write(output+32,32,const_cast<dig_t*>(g->y));
}

//Used to implement cout << G1 and fromString
void G1::fromStream(istream &in, int base) {
    if (base < 256) {
        vector<string> o = streamtok(in);
        fp_read_str(g->x,o[0].c_str(),o[0].length(),base);
        fp_read_str(g->y,o[1].c_str(),o[1].length(),base);
        fp_read_str(g->z,o[2].c_str(),o[2].length(),base);
    }
    else {
        char o[64];
        for (int i = 0; i < 64; i++) { o[i] = (char)in.get(); }
        fromBin(o);
    }
}

G1::~G1() {
  g1_free(g);
}
