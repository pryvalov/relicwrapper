#include <iostream>

#include "Pairing.h"
//#undef __cplusplus
extern "C" {
#include <relic/relic.h>
}
//#define __cplusplus 1

using namespace std;

int main(int argc, char **argv)
{
  Pairing e;
  if (string(argv[1]) == "test") {
    G1 g(e,"1234567",7);
    string u = g.toString(256);
    G1 h;
    h.fromString(u,256);
    cout << g << " " << h << "\n";
  }
  else if (string(argv[1]) != "read") {
    int base = (string(argv[1]) == "write") ? 256 : (string(argv[1]) == "64") ? 64 : 10;
    G1 g(e,"1234567",7);
    g.dump(stdout,"",base);
    G2 h(e,"1234567",7);
    h.dump(stdout,"",base);
    GT i = e(g,h);
    i.dump(stdout,"",base);
    Zr n = Zr(1234567);
    n.dump(stdout,"",base);
  }
  else {
    G1 g;
    g.fromStream(cin,256);
    G2 h;
    h.fromStream(cin,256);
    GT i = e(g,h);
    Zr n;
    n.fromStream(cin,256);
    cout << g.toString(16) << "\n";
    cout << h.toString(16) << "\n";
    cout << i.toString(16) << "\n";
    cout << n.toString(16) << "\n";
    G1 go(e,"1234567",7);
    G2 ho(e,"1234567",7);
    GT io = e(go,ho);
    Zr no = Zr(1234567);
    cout << go.toString(16) << "\n";
    cout << ho.toString(16) << "\n";
    cout << io.toString(16) << "\n";
    cout << no.toString(16) << "\n";
  }
  return 0;
}
