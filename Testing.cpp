#include <iostream>

#include "Pairing.h"
//#undef __cplusplus
extern "C" {
#include <relic/relic.h>
}
//#define __cplusplus 1

using namespace std;

void test_G1(){
	Pairing e;
	G1 g1 = e.get_g1_gen();
	Zr a(e, 0);
	cout << "g1 = " << g1 << endl;
	g1 *= G1();
	cout << "g1 * identity = " << g1 << endl;
}

int main(int argc, char **argv)
{
  Pairing e;
  cout<<"Is symmetric? "<< e.isSymmetric()<< endl;
  cout<<"Is pairing present? "<< e.isPairingPresent()<< endl;  

  G1 gen1 = e.get_g1_gen();
  G2 gen2 = e.get_g2_gen();
  GT gent = e.get_gt_gen();
  cout << ((e(gen1,gen2) == gent) ? "Generators work\n" : "Generator pairing does not match\n");

  G1 p(e,false);
  p = G1(e, "1234567", 7);
  p.dump(stdout,"Hash for 1234567 is ",10);
  G2 q(e,false);
  Zr r(e,(long int)10121);
  (Zr(e,0)-Zr(e,1)).dump(stdout,"negone",10);
  (Zr(e,1)/Zr(e,2)).dump(stdout,"half",10);
  (Zr(e,1)/Zr(e,77)).dump(stdout,"1/77",10);
  (Zr(e,1001)/Zr(e,77)).dump(stdout,"13",10);
  r.dump(stdout,"r",10);
  // Create a random element of Zr
  Zr s(e,true);
  s.dump(stdout,"s",10);
  r =s;
  r.dump(stdout,"new r",10);
  GT LHS = e(p,q)^r;
  G1 pr(p^r);
  p.dump(stdout,"p", 10);
  q.dump(stdout, "q", 10);
  pr.dump(stdout,"p^r", 10);
  GT RHS = e((p^r),q);
  LHS.dump(stdout,"LHS", 64);
  RHS.dump(stdout,"RHS", 64);

  if((e(p,q)^r) == e(p^r,q))
	cout<<"Correct Pairing Computation"<<endl;
  else
	cout<<"Incorrect Pairing Computation"<<endl;
  if((p.inverse()).square() == (p.square()).inverse())
	cout<<"Inverse, Square works"<<endl;
  else
	cout<<"Inverse, Square does not work."<<endl;
  G1 a;
  a = p;
  p.dump(stdout,"p is ") ;
  a.dump(stdout,"a is ") ;
  // Create the identity element b (in the same group as a)
  G1 b(a,true);
  b.dump(stdout,"b is ") ;

  test_G1();

  return 0;
}
