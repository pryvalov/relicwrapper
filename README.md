# RelicWrapper

This is a forked version maintained by Ivan Pryvalov <ivan.pryvalov@uni.lu>.
Contributions from Aniket Kate <aniket@purdue.edu>.

Starting from July 2021, support for python2 is dropped and only python3 is supported. This may require to install boost-python3 instead of boost-python.


-------------------------------------------

relicwrapper 0.9 [May 21, 2013]

Vitalik Buterin <v2buteri@uwaterloo.ca> 
Contributions from Ian Goldberg <iang@cs.uwaterloo.ca>

This project provides C++ and python wrappers to the RELIC library for
bilinear pairings.

You will need boost and boost-python, as well as RELIC (0.3.4 is
recommended).

"make" will build the C++ wrapper.  
"make python" will build the python wrapper. 

See Testing.cpp, Testing2.cpp, and test.py for examples of use.

## Relic

      $ git clone https://github.com/relic-toolkit/relic && cd relic
      $ cmake .
      $ make
      # make install

The following files should be copied to the system:

      /usr/local/include/relic/*
      /usr/local/cmake/relic-config.cmake
      /usr/local/lib/librelic.so
      /usr/local/lib/librelic_s.a


## Licensing

Copyright (C) 2013 Vitalik Buterin, Ian Goldberg

Copyright (C) 2013-2017 Ivan Pryvalov, Aniket Kate

Copyright (C) 2021,2023 Ivan Pryvalov

relicwrapper is free software: you can redistribute it and/or modify it
under the terms of version 3 of the GNU Lesser General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License and the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU General Public License and
the GNU Lesser General Public License along with this program.  If not,
see <http://www.gnu.org/licenses/>.
