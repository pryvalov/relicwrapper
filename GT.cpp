#include "GT.h"

extern "C" {
#include <relic/relic.h>
}

using namespace std;

void GT::init() { gt_new(g);}

//Basic initialization to one
GT::GT() {
  init();
  gt_set_unity(g);
}

GT::GT(const Pairing &e) {
  init();
  gt_set_unity(g);
}

//identity = true  -> one
//identity = false -> random value
GT::GT(const Pairing &e, bool identity) {
  boolInit(identity);
}

GT::GT( bool identity) {
  boolInit(identity);
}

//identity = true  -> one
//identity = false -> copy constructor
GT::GT(const GT &h, bool identity) {
  if (identity) {
    boolInit(true);
  }
  else {
    init();
    WITH_TEMP(gt,temp, {
      h.getElement(temp);
      setElement(temp);
    })
  }
}

void GT::boolInit(bool identity) {
  init();
  if (identity) {
    gt_set_unity(g);
  }
  else {
    gt_rand(g);
  }
}

//Init from string
GT::GT(const string &s, unsigned short base) {
  init();
  fromString(s,base);
}

GT GT::randomize() { gt_rand(g); GT o; o.setElement(g); return o; }

bool GT::isIdentity() const {
  WITH_TEMP_RETURN(gt,temp, {
    gt_set_unity(const_cast<fp6_t(*)>(temp));
    ans = gt_cmp(const_cast<fp6_t(*)>(g),const_cast<fp6_t(*)>(temp)) == 0;
  },bool,ans)
}

//Arithmetic operators
GT& GT::operator*=(const GT& other) {
  WITH_TEMP(gt,temp, {
    other.getElement(temp);
    gt_mul(g,g,temp);
  })
  return *this;
}

GT& GT::operator/=(const GT& other) {
  WITH_TEMP(gt,temp, {
    other.getElement(temp);
    gt_inv(temp,temp);
    gt_mul(g,g,temp);
  })
  return *this;
}

GT& GT::operator^=(const Zr& other) {
  WITH_TEMP(bn,temp, {
    other.getElement(temp);
    gt_exp(g,g,temp);
  })
  return *this;
}
bool GT::operator==(const GT& other) const {
  WITH_TEMP_RETURN(gt,temp, {
    other.getElement(const_cast<fp6_t(*)>(temp));
    ans = gt_cmp(const_cast<fp6_t(*)>(g),const_cast<fp6_t(*)>(temp)) == 0;
  },bool,ans)
}

//Assignment operator
void GT::operator=(const GT& other) {
  WITH_TEMP(gt,temp, {
    other.getElement(temp);
    setElement(temp);
  })
}

//Outputs to string, readable with fromString
string GT::toString(unsigned short base) const{
    char x[12][128];
    for (int i = 0; i < 12; i++) {
        fp_write_str(x[i],257,(const_cast<fp6_t(*)>(g))[i/6][(i%6)/2][i%2],base);
    }
    char o1[6][333];
    for (int i = 0; i < 6; i++) {
      sprintf(o1[i],(base == 10) ? "[%s,%s]" : "[\"%s\",\"%s\"]",x[i*2],x[i*2+1]);
    }
    char o[2000];
    sprintf(o,"[[%s,%s,%s],[%s,%s,%s]]",o1[0],o1[1],o1[2],o1[3],o1[4],o1[5]);
    string outstr(o);
    return outstr;
}

void GT::toBin(char* output) const {
    for (int j = 0; j < 12; j++) {
        FP_bin_write(output+32*j,32,const_cast<dig_t*>(g[j/6][(j%6)/2][j%2]));
    }
}
void GT::fromBin(const char* input) {
    for (int j = 0; j < 12; j++) {
        FP_bin_read(g[j/6][(j%6)/2][j%2],const_cast<char*>(input+32*j),32);
    }
}

//Used to implement cout << GT and fromString
void GT::fromStream(istream &in, int base) {
  if (base < 256) {
    vector<string> o = streamtok(in);
    for (int j = 0; j < 12; j++) {
        fp_read_str(g[j/6][(j%6)/2][j%2],o[j].c_str(),o[j].length(),base);
    }
  }
  else {
    char o[384];
    for (int i = 0; i < 384; i++) { o[i] = (char)in.get(); }
    fromBin(o);
  }
}

GT::~GT() {
  gt_free(g);
}
