
#ifndef __G2_H__
#define __G2_H__

#include "Zr.h"
#include "G.h"

//#undef __cplusplus
extern "C" {
#include <relic/relic.h>
}
//#define __cplusplus 1

class G2 : public G{
public:
  G2();
  virtual ~G2();

  //Basic initialization to point at infinity
  G2(const Pairing &e);

  //identity = true  -> point at infinity
  //identity = false -> random value
  G2(const Pairing &e, bool identity);
  G2(bool identity);

  //identity = true  -> point at infinity
  //identity = false -> copy constructor
  G2(const G2 &h, bool identity = false);

  short getElementSize (bool compressed=false) const {
    return 128;
  }

  bool isIdentity() const;

  //Init from hash
  G2(const Pairing &e, const void *data, unsigned short len);

  //Init from string
  G2(const std::string &s, unsigned short base = 10);

  //Arithmetic operators
  G2& operator*=(const G2& other);
  G2& operator/=(const G2& other);
  G2& operator^=(const Zr& other);

  const G2 operator*(const G2& other) const { G2 tempg = *this; return tempg *= other; }
  const G2 operator/(const G2& other) const { G2 tempg = *this; return tempg /= other; }
  const G2 operator^(const Zr& other) const { G2 tempg = *this; return tempg ^= other; }
  bool operator==(const G2& other) const;
  bool operator!=(const G2& other) const { return !(*this==other);}

  const G2 square() const {
    return *this * *this;
  }
  const G2 inverse() const {
    return G2(true) / *this;
  }
  void operator=(const G2& other);

  //String and I/O methods
  std::string toString (unsigned short base=10) const;
  std::string decimalToString () const { return toString(10); }

  void fromString(const std::string &input, unsigned short base = 10) {
	  std::istringstream iss(input);
    fromStream(iss,base);
  }

  void fromBin(const char* input);
  void toBin(char* output) const;

  friend inline std::ostream& operator<<(std::ostream &out, const G2& x) {
    out << x.toString(10);
    return out;
  }

  friend inline std::istream& operator>>(std::istream &in, G2& out) {
    out.fromStream(in,10);
    return in;
  }

  void fromStream(std::istream &in, int base);

  //Helper functions
  void getElement (g2_t out) const {
    g2_copy(out,const_cast<ep2_st*>(g));
  }
  void setElement (g2_t in) {
    g2_copy(g,in);
  }
  G2 randomize();
  void init();
protected:
  g2_t g;
  void boolInit (bool identity);
};
#endif
