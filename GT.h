
#ifndef __GT_H__
#define __GT_H__

#include "Zr.h"
#include "G.h"

//#undef __cplusplus
extern "C" {
#include <relic/relic.h>
}
//#define __cplusplus 1

class GT : public G{
public:
  GT();
  virtual ~GT();

  //Basic initialization to one
  GT(const Pairing &e);

  //identity = true  -> one
  //identity = false -> random value
  GT(const Pairing &e, bool identity);
  GT(bool identity);

  //identity = true  -> one
  //identity = false -> copy constructor
  GT (const GT &h, bool identity = false);

  //Init from string
  GT(const std::string &s, unsigned short base = 10);

  //Arithmetic operators
  GT& operator*=(const GT& other);
  GT& operator/=(const GT& other);
  GT& operator^=(const Zr& other);

  const GT operator*(const GT& other) const { GT temp = *this; return temp *= other; }
  const GT operator/(const GT& other) const { GT temp = *this; return temp /= other; }
  const GT operator^(const Zr& other) const { GT temp = *this; return temp ^= other; }
  bool operator==(const GT& other) const;
  bool operator!=(const GT& other) const { return !(*this==other);}

  const GT square() {
    GT a = GT();
    a.setElement(g);
    return a ^ Zr(2);
  }
  const GT inverse() {
    GT temp = GT(true);
    temp /= *this;
    return temp;
  }
  void operator=(const GT& other);

  //String and I/O methods
  std::string toString (unsigned short base=10) const;
  std::string decimalToString () const { return toString(10); }

  void fromString(const std::string &input, unsigned short base = 10) {
	  std::istringstream iss(input);
    fromStream(iss,base);
  }

  void fromBin(const char* input);
  void toBin(char* output) const;

  friend inline std::ostream& operator<<(std::ostream &out, const GT& x) {
    out << x.toString(10);
    return out;
  }

  friend inline std::istream& operator>>(std::istream &in, GT& out) {
    out.fromStream(in,10);
    return in;
  }

  void fromStream(std::istream &in, int base);

  //Helper functions
  void getElement (gt_t out) const {
    gt_copy(out,const_cast<fp6_t(*)>(g));
  }
  void setElement (gt_t in) {
    gt_copy(g,in);
  }
  GT randomize();
  void init();

protected:
  gt_t g;
  void boolInit (bool identity);
  bool isIdentity() const;
};
#endif
