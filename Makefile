COMMAND=g++
SRCS=$(wildcard *.cpp)
#LOCAL_HOME = venv_cpp/
#INCLUDE = -I${LOCAL_HOME}/include
LIBPATH = -L. #-L${LOCAL_HOME}/lib

CXXFLAGS= ${INCLUDE} -g -O0 -Wall -fPIC $(ARCH)
PYTHONINC = `python-config --includes`
PYTHONLDFLAGS = `python-config --ldflags`
BOOST_INC = /usr/include
BOOST_LIB = /usr/lib
PY_TARGET = pyrelic
TARGET = relicwrapper
#LIB_BOOST_PYTHON = boost_python   # outdated python2 ?
#LIB_BOOST_PYTHON = boost_python3  # arch
LIB_BOOST_PYTHON = boost_python310  # ubuntu 2022.04



all: Testing Testing2 librelicwrapper.a #lib$(TARGET).so
python: librelicwrapper.a $(PY_TARGET).so

COMMON_OBJS=G.o G1.o G2.o GT.o Zr.o Pairing.o

Testing: Testing.cpp librelicwrapper.a
	g++  -g -Wall -o $@ Testing.cpp ${LIBPATH} -lrelicwrapper -lrelic $(ARCH) ${CXXFLAGS}

Testing2: Testing2.cpp librelicwrapper.a
	g++  -g -Wall -o $@ Testing2.cpp ${LIBPATH} -lrelicwrapper -lrelic $(ARCH) ${CXXFLAGS}
	

librelicwrapper.a: $(COMMON_OBJS)
	ar rcs $@ $^

lib$(TARGET).so: $(COMMON_OBJS) 
	g++ -shared -Wl,-soname,lib${TARGET}.so \
	-o lib${TARGET}.so \
	${COMMON_OBJS}

$(PY_TARGET).so: $(PY_TARGET).o
	g++ -shared -Wl,-soname,$(PY_TARGET).so \
	$(PY_TARGET).o -L$(BOOST_LIB) -l${LIB_BOOST_PYTHON} -fPIC \
	$(PYTHONLDFLAGS) \
	-o $(PY_TARGET).so -I. $(LIBPATH) -lrelicwrapper -lrelic $(ARCH)

$(PY_TARGET).o: $(PY_TARGET).cpp librelicwrapper.a
	g++ $(PYTHONINC) -I$(BOOST_INC) -c $(PY_TARGET).cpp -fPIC $(INCLUDE) -L. -lrelicwrapper -lrelic $(ARCH)


# DO NOT DELETE

G1.o: G1.h G.h Pairing.h Zr.h
G2.o: G2.h G.h Pairing.h Zr.h
G.o: G.h Pairing.h Zr.h
GT.o: GT.h G.h Pairing.h Zr.h
Pairing.o: Pairing.h G1.h G.h Zr.h G2.h GT.h
Testing.o: relicwrapper.h G1.h G.h Pairing.h Zr.h G2.h GT.h
Zr.o: Zr.h Pairing.h

clean:
	rm -f *o librelicwrapper.a Testing Testing2 TestingZp $(PY_TARGET).so lib$(TARGET).so
