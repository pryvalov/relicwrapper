from pyrelic import *

e = Pairing()

a = Zr(1)
assert str(a) == "1"
b = Zr(77)
c = a/b
d = (a/b) * Zr(1001)
print a
print b
print c
print d
assert str(d) == "13"
d2 = Zr(3) ^ Zr(10)
print d2
assert str(d2) == "59049"
f = (Zr(12321) + Zr(87678)) * Zr(2)
print f
assert str(f) == "199998"
g = Zr(False)
print g
assert str(g) == "0"
h = Zr(0).randomize()
print h
assert len(str(h)) > 30 # This may accidentally fail once or twice in the lifetime of the universe
print "Zr tests passed"

u = G1(True)
assert len(str(u)) < 15
v = G1(True).randomize()
assert len(str(v)) > 30
print (v^Zr(3)) ^ Zr(3)
print v^Zr(9)
assert (v ^ Zr(3)) ^ Zr(3) == v ^ Zr(9)

u = G2(True)
assert len(str(u)) < 30
w = G2(False)
assert len(str(w)) > 60
print (w^Zr(3)) ^ Zr(3)
print w^Zr(9)
assert (w ^ Zr(3)) ^ Zr(3) == w ^ Zr(9)
print "G1 and G2 tests passed"

a = e(v,w)
print a
b = e(v^Zr(454),w)
print b
c = e(v,w^Zr(454))
print c
d = a^Zr(454)
print d
assert str(a) != str(b)
assert str(b) == str(c) == str(d)
print "Pairing tests passed"
