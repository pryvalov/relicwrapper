
#ifndef __G1_H__
#define __G1_H__

#include "Zr.h"
#include "G.h"

//#undef __cplusplus
extern "C" {
#include <relic/relic.h>
}
//#define __cplusplus 1


class G1 : public G{
public:
  G1();
  virtual ~G1();

  //Basic initialization to point at infinity
  G1(const Pairing &e);

  //identity = true  -> point at infinity
  //identity = false -> random value
  G1(const Pairing &e, bool identity);
  G1(bool identity);

  //identity = true  -> point at infinity
  //identity = false -> copy constructor
  G1(const G1& h, bool identity = false);

  short getElementSize (bool compressed=false) const {
    return 64;
  }

  bool isIdentity() const;

  //Init from hash
  G1(const Pairing &e, const void *data, unsigned short len);

  //Init from string
  G1(const std::string &s, unsigned short base = 10);

  //Arithmetic operators
  G1& operator*=(const G1& other);
  G1& operator/=(const G1& other);
  G1& operator^=(const Zr& other);

  const G1 operator*(const G1& other) const { G1 tempg = *this; return tempg *= other; }
  const G1 operator/(const G1& other) const { G1 tempg = *this; return tempg /= other; }
  const G1 operator^(const Zr& other) const { G1 tempg = *this; return tempg ^= other; }
  bool operator==(const G1& other) const;
  bool operator!=(const G1& other) const { return !(*this==other);}

  const G1 square() const {
    return *this * *this;
  }
  const G1 inverse() const {
    return G1(true) / *this;
  }
  void operator=(const G1& other);

  //String and I/O methods
  std::string toString (unsigned short base=10) const;
  std::string decimalToString () const { return toString(10); }

  void fromString(const std::string &input, unsigned short base = 10) {
	  std::istringstream iss(input);
    fromStream(iss,base);
  }

  void fromBin(const char* input);
  void toBin(char* output) const;

  friend inline std::ostream& operator<<(std::ostream &out, const G1& x) {
    out << x.toString(10);
    return out;
  }
  friend inline std::istream& operator>>(std::istream &in, G1& out) {
    out.fromStream(in,10);
    return in;
  }
  void fromStream(std::istream &in, int base);

  //Helper functions
  void getElement (g1_t out) const {
    g1_copy(out,const_cast<ep_st*>(g));
  }
  void setElement (g1_t in) {
    g1_copy(g,in);
  }
  G1 randomize();
  void init();

protected:
  g1_t g;
  void boolInit (bool identity);
};


#endif
