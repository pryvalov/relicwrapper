#ifndef __RELICWRAPPER_H__
#define __RELICWRAPPER_H__

#define PPPairing Pairing
#define PPCAT(A, B) A ## B
#define WITH_TEMP(tp, nm, cd) PPCAT(tp, _t) nm; PPCAT(tp, _new) (nm); cd; PPCAT(tp, _free) (nm);
#include "G1.h"
#include "G2.h"
#include "G.h"
#include "GT.h"
#include "Pairing.h"
#include "Zr.h"

#endif /* __RELICWRAPPER_H__ */
