#ifndef __G_H__
#define __G_H__

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>

#define PPCAT(A, B) A ## B
#define WITH_TEMP(tp, nm, cd) PPCAT(tp, _t) nm; PPCAT(tp, _new) (nm); cd; PPCAT(tp, _free) (nm);
#define WITH_TEMP_RETURN(tp, nm, cd, rtp, ans) rtp ans; PPCAT(tp, _t) nm; PPCAT(tp, _new) (nm); cd; PPCAT(tp, _free) (nm); return ans;

//#undef __cplusplus
extern "C" {
#include "relic/relic.h"
}
//#define __cplusplus 1

class Pairing;

std::vector<std::string> streamtok(std::istream &in);
void FP_bin_write(char* c,int len,fp_t x);
void FP_bin_read(fp_t x,char* c,int len);

class G {
public:
  G() { }
//  virtual ~G();
  virtual std::string toString (unsigned short base=10) const { return ""; };
  virtual void toBin (char* output) const { };
  virtual short getElementSize (bool compressed=false) const { return 0; };
  virtual void fromString (const std::string &input, unsigned short base=10) const { };
  void dump(FILE *f, const char *label="",
			 unsigned short base=10) const {
    if (base < 256) {
      if (label) fprintf(f, "%s: ", label);
      fprintf(f,"%s\n",toString(base).c_str());
    }
    else {
      char o[1000]; toBin(o);
      short g = getElementSize();
      for (int i = 0; i < g; i++) { fputc(o[i],f); }
    }
  }

  std::string toString (int base) const { return toString((unsigned short) base); }
  std::string toString (bool compressed) const { return toString(10); }

  bool isElementPresent() const { return true; }
};

#endif
