#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Pairing.h"
#include "G1.h"
#include "G2.h"
#include "GT.h"

extern "C" {
#include <relic/relic.h>
}

using namespace std;

int coresInitialized = 0;

Pairing::Pairing(const Pairing &g1) {
    initPairing();
}

Pairing& Pairing::operator=(const Pairing &rhs) {
    return *this;
}

const GT Pairing::operator() ( G1 p, G2 q) const {
    g1_t pe;
    g1_new(pe);
    g2_t qe;
    g2_new(qe);
    p.getElement(pe);
    q.getElement(qe);
    gt_t o;
    gt_new(o);
    pc_map(o,pe,qe);
    GT ans;
    ans.setElement(o);
    g1_free(pe);
    g2_free(qe);
    gt_free(o);
    return ans;
}

const GT Pairing::apply ( G1 p,  G2 q) const {
    return (*this)(p,q);
}

void initPairing() {
    if (coresInitialized == 0) {
        cerr << "Initializing core\n";
        core_init();
        pc_param_set_any();
    }
    coresInitialized++;
}
void delPairing() {
    coresInitialized--;
    if (coresInitialized == 0) {
        core_clean();
    }
}
