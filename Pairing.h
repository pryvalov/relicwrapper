#ifndef __PAIRING_H__
#define __PAIRING_H__

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "G1.h"
#include "G2.h"
#include "GT.h"

//#undef __cplusplus
extern "C" {
#include <relic/relic.h>
}
//#define __cplusplus 1

void initPairing();
void delPairing();

typedef enum {Type_G1, Type_G2, Type_GT, Type_Zr} PairingElementType;

class Pairing {
public:
    Pairing() { initPairing(); }
    ~Pairing() { delPairing(); }

    void init (const char * pairingparams) {}

    Pairing(const char * buf, size_t len) { initPairing(); }
    Pairing(const char * buf) { initPairing(); }
    Pairing(const std::string &buf) { initPairing(); }
    Pairing(const FILE * buf) { initPairing(); }

    size_t getElementSize(PairingElementType type,
                            bool compressed = false) const { return 256; }

    //Copy constructor
    Pairing(const Pairing &g1);

    // Assignment operator: 
    Pairing& operator=(const Pairing &rhs);
    
    bool isSymmetric() const {
      return false;
    }
    const GT operator()(const G1 p, const G2 q) const;
    const GT apply(const G1 p, const G2 q) const;
    bool isPairingPresent() { return true; }
    // Get the order of the groups that we're using
    // Note: this will NOT play nicely with
    // arithmetic functions since all other Zrs are
    // in the prime field modulo this value
    Zr get_ord() const {
      bn_t a;
      bn_new(a);
      g1_get_ord(a);
      Zr n = Zr(0);
      n.setElement(a);
      bn_free(a);
      return n;
    }
    //Canonical generators
    G1 get_g1_gen() const { return G1(*this,"",0); }
    G2 get_g2_gen() const { return G2(*this,"",0); }
    GT get_gt_gen() const { return apply(get_g1_gen(),get_g2_gen()); }
};

#endif
