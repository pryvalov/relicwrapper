#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstring>
#include <assert.h>

#include "relicwrapper.h"

//#undef __cplusplus
extern "C" {
#include <relic/relic.h>
}
//#define __cplusplus 1

using namespace std;

int main() {
  Pairing e;
  Zr v = Zr(e,true);
  Zr u = Zr(e,12321);
  u.dump(stdout,"12321 is",10);
  v.dump(stdout,"Zr el is",10);
  string s12 = u.toString(12);
  printf("Base 12 is %s\n",s12.c_str());
  Zr vv(e, false);
  vv.fromString(s12,12);
  vv.dump(stdout,"Zr back from base 12 is",10);
  Zr vvv = Zr(e,87678);
  Zr vvvv = Zr(e,2);
  //Zr vvvvv = vvvv;
  //vvvvv *= (vvv + vv);
  Zr ans(e, false);
  ans = (vv + vvv) * vvvv;
  ans.dump(stdout,"199998 is",10);
  Zr base(e, 3);
  Zr exp(e, 10);
  Zr res = base ^ exp;
  res.dump(stdout,"59049 is",10);
  G1 a(e, "word",4);
  G1 alta1(e, "word",4);
  G1 alta2(e, "wyrd",4);
  string check1 = (a == alta1) ? "TRUE" : "FALSE";
  string check2 = (a == alta2) ? "TRUE" : "FALSE";
  G2 b(e, "word",4);
  G2 altb1 (e,"word",4);
  G2 altb2 (e,"wyrd",4);
  string check3 = (b == altb1) ? "TRUE" : "FALSE";
  string check4 = (b == altb2) ? "TRUE" : "FALSE";
  a.dump(stdout,"o",10);
  alta1.dump(stdout,"o",10);
  alta2.dump(stdout,"o",10);
  b.dump(stdout,"o",10);
  altb1.dump(stdout,"o",10);
  altb2.dump(stdout,"o",10);
  cout << "Hashing to points.\nG1 Comparison check: " + check1 + " (should be TRUE)\n";
  cout << "Comparison check: " + check2 + " (should be FALSE)\n";
  cout << "Hashing to points.\nG2 Comparison check: " + check3 + " (should be TRUE)\n";
  cout << "Comparison check: " + check4 + " (should be FALSE)\n";
  a.dump(stdout,"G1 el is",10);
  b.dump(stdout,"G2 el is",10);
  G2 bv = b ^ v;
  G1 av = a ^ v;
  av.dump(stdout,"G1*v is",10);
  bv.dump(stdout,"G2*v is",10);
  GT c1 = e(a,bv);
  c1.dump(stdout,"GT el 1 is",10);
  GT c2 = e(av,b);
  c2.dump(stdout,"GT el 2 is",10);
  GT c3 = e(a,b) ^ v;
  GT c4 = e(a,b);
  c3.dump(stdout,"GT el 3 is",10);
  c4.dump(stdout,"GT el 4 is",10);
  check1 = (c1 == c2) ? "TRUE" : "FALSE";
  check2 = (c1 == c3) ? "TRUE" : "FALSE";
  check3 = (a == av) ? "TRUE" : "FALSE";
  check4 = (c3 == c4) ? "TRUE" : "FALSE";
  cout << "Bilinearity tests.\nComparison check: " + check1 + " (should be TRUE)\n";
  cout << "Comparison check: " + check2 + " (should be TRUE)\n";
  cout << "Comparison check: " + check3 + " (should be FALSE)\n";
  cout << "Comparison check: " + check4 + " (should be FALSE)\n";

  G1 asq = a.square();
  G1 ainv = a.inverse();
  G1 ainvsq1 = asq.inverse();
  G1 ainvsq2 = ainv.square();
  ainvsq1.dump(stdout,"Inverse square one way is",10);
  ainvsq2.dump(stdout,"Inverse square another way is",10);
  check1 = (ainvsq1 == ainvsq2) ? "TRUE" : "FALSE";
  cout << "Comparison check: " + check1 + " (should be TRUE)\n";

  string s = a.toString(16);
  G1 afroms;
  afroms.fromString(s,16);
  a.dump(stdout,"Original G1",10);
  afroms.dump(stdout,"G1 to and from string",10);
  string t = b.toString(16);
  G2 bfroms;
  bfroms.fromString(t,16);
  b.dump(stdout,"Original G2",10);
  bfroms.dump(stdout,"G2 to and from string",10);
  string w = c1.toString(16);
  GT cfroms;
  cfroms.fromString(w,16);
  c1.dump(stdout,"Original GT",10);
  cfroms.dump(stdout,"GT to and from string",10);
  check1 = (a == afroms) ? "TRUE" : "FALSE";
  check2 = (b == bfroms) ? "TRUE" : "FALSE";
  check3 = (c1 == cfroms) ? "TRUE" : "FALSE";
  cout << "Comparison check: " + check1 + " (should be TRUE)\n";
  cout << "Comparison check: " + check2 + " (should be TRUE)\n";

  assert (a==afroms);
  assert (b==bfroms);
  assert (c1==cfroms);
}
