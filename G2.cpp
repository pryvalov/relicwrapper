#include "G2.h"

extern "C" {
#include <relic/relic.h>
}

using namespace std;

void G2::init() { g2_new(g); }

//Basic initialization to point at infinity
G2::G2() {
  init();
  g2_set_infty(g);
}

G2::G2(const Pairing &e) {
  init();
  g2_set_infty(g);
}

//identity = true  -> point at infinity
//identity = false -> random value
G2::G2(const Pairing &e, bool identity) {
  boolInit(identity);
}

G2::G2(bool identity) {
  boolInit(identity);
}

//identity = true  -> point at infinity
//identity = false -> copy constructor
G2::G2(const G2 &h, bool identity) {
  if (identity) {
    boolInit(true);
  }
  else {
    init();
    WITH_TEMP(g2,temp, {
      h.getElement(temp);
      setElement(temp);
    })
  } 
}

void G2::boolInit(bool identity) {
  init();
  if (identity) {
    g2_set_infty(g);
  }
  else {
    g2_rand(g);
  }
}

G2 G2::randomize() { g2_rand(g); G2 o; o.setElement(g); return o; }

bool G2::isIdentity() const {
  WITH_TEMP_RETURN(g2,temp, {
    g2_set_infty(temp);
    ans = g2_cmp(const_cast<ep2_st*>(g),temp) == 0;
  },bool,ans)
}

//Arithmetic operators
G2& G2::operator*=(const G2& other) {
  WITH_TEMP(g2,temp, {
    other.getElement(temp);
    g2_add(g,g,temp);
    g2_norm(g,g);
  })
  return *this;
}

G2& G2::operator/=(const G2& other) {
  WITH_TEMP(g2,temp, {
    other.getElement(temp);
    g2_sub(g,g,temp);
    g2_norm(g,g);
  })
  return *this;
}

G2& G2::operator^=(const Zr& other) {
  WITH_TEMP(bn,temp, {
    other.getElement(temp);
    g2_mul(g,g,temp);
    g2_norm(g,g);
  })
  return *this;
}

bool G2::operator==(const G2& other) const {
  WITH_TEMP_RETURN(g2,temp, {
    other.getElement(temp);
    ans = g2_cmp(const_cast<ep2_st*>(g),const_cast<ep2_st*>(temp)) == 0;
  },bool,ans)
}

//Assignment operator
void G2::operator=(const G2& other) {
  WITH_TEMP(g2,temp, {
    other.getElement(temp);
    setElement(temp);
  })
}

//Init from hash
G2::G2(const Pairing &e, const void *data, unsigned short len) {
  init();
  unsigned char cdata[len];
  memcpy(cdata,data,len);
  g2_map(g,cdata,len);
}

//Init from string
G2::G2(const string &s, unsigned short base) {
  init();
  fromString(s,base);
}

//Outputs to string, readable with fromString
string G2::toString(unsigned short base) const {
  if (base < 256) {
    char x[2][257], y[2][257], z[2][257];
    ep2_st* h = const_cast<ep2_st*>(g);
    fp_write_str(x[0],257,h->x[0],base);
    fp_write_str(x[1],257,h->x[1],base);
    fp_write_str(y[0],257,h->y[0],base);
    fp_write_str(y[1],257,h->y[1],base);
    fp_write_str(z[0],257,h->z[0],base);
    fp_write_str(z[1],257,h->z[1],base);
    char o[1000];
    const char* format = (base == 10) ? "[[%s,%s],[%s,%s],[%s,%s]]" 
                 : "[[\"%s\",\"%s\"],[\"%s\",\"%s\"],[\"%s\",\"%s\"]]";
    sprintf(o,format,x[0],x[1],y[0],y[1],z[0],z[1]);
    string outstr(o);
    return outstr;
  }
  else {
    char o[128]; toBin(o); return string(o,128);
  }
}

//Base-256 methods
void G2::fromBin(const char* input) {
    FP_bin_read(g->x[0],const_cast<char*>(input),32);
    FP_bin_read(g->x[1],const_cast<char*>(input)+32,32);
    FP_bin_read(g->y[0],const_cast<char*>(input)+64,32);
    FP_bin_read(g->y[1],const_cast<char*>(input)+96,32);
    char buffer[64];
    for (int i = 0; i < 64; i++) buffer[i] = (i==31) ? '\x01' : '\x00';
    FP_bin_read(g->z[0],buffer,32);
    FP_bin_read(g->z[1],buffer+32,32);
}
void G2::toBin(char* output) const {
    FP_bin_write(output,32,const_cast<dig_t*>(g->x[0]));
    FP_bin_write(output+32,32,const_cast<dig_t*>(g->x[1]));
    FP_bin_write(output+64,32,const_cast<dig_t*>(g->y[0]));
    FP_bin_write(output+96,32,const_cast<dig_t*>(g->y[1]));
}

//Used to implement cout << G2 and fromString
void G2::fromStream(istream &in, int base) {
    if (base < 256) {
        vector<string> o = streamtok(in);
        fp_read_str(g->x[0],o[0].c_str(),o[0].length(),base);
        fp_read_str(g->x[1],o[1].c_str(),o[1].length(),base);
        fp_read_str(g->y[0],o[2].c_str(),o[2].length(),base);
        fp_read_str(g->y[1],o[3].c_str(),o[3].length(),base);
        fp_read_str(g->z[0],o[4].c_str(),o[4].length(),base);
        fp_read_str(g->z[1],o[5].c_str(),o[5].length(),base);
    }
    else {
        char o[128];
        for (int i = 0; i < 128; i++) { o[i] = (char)in.get(); }
        fromBin(o);
    }
}

G2::~G2() {
  g2_free(g);
}
