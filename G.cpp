#include "G1.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <vector>

extern "C" {
#include "relic/relic.h"
}

using namespace std;


vector<string> streamtok(istream &in) {
  //Grabs a complete bracketed expression and converts it into a vector of strings
  //eg. [[[12124,123123],[[5,6,7],8]],[9,10]]
  //Ignores spaces
  vector<string> v;
  int depth = 0;
  string cur = "";
  char o[2];
  do {
    do { in.read(o,1);
     } while (o[0] == ' ' || o[0] == '\n' || o[0] == '\t');
    if (o[0] == '[') {
      if (cur != "") { v.push_back(cur); cur = ""; }
      depth++;
    }
    else if (o[0] == ']') {
      if (cur != "") { v.push_back(cur); cur = ""; }
      depth--;
    }
    else if (o[0] == ',') {
      if (cur != "") { v.push_back(cur); cur = ""; }
    }
    else cur += o[0];
  } while (depth > 0);
  return v;
}
void FP_bin_write(char* c,int len,fp_t x) {
  WITH_TEMP(bn,temp, {
    fp_prime_back(temp,x);
    bn_write_bin((unsigned char*)c,len,temp);
  })
}

void FP_bin_read(fp_t x,char* c,int len) {
  WITH_TEMP(bn,temp, {
    bn_read_bin(temp,(unsigned char*)c,len);
    if (bn_is_zero(temp)) { fp_zero(x); }
    else {
      if (temp->used == 1) fp_prime_conv_dig(x, temp->dp[0]);
      else fp_prime_conv(x, temp);
    }
  })
}
