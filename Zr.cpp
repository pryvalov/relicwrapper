#include "Zr.h"
#include "Pairing.h"
#include <iostream>

extern "C" {
#include <relic/relic.h>
}

using namespace std;

Zr::Zr(const Pairing &e, long long int i) {
	initWithNum((unsigned long long int) i);
	setOrder(e.get_ord());
}

Zr::Zr(const Pairing &e, long int i) {
	initWithNum((unsigned long long int) i);
	setOrder(e.get_ord());
}

Zr::Zr(const Pairing &e, int i) {
	initWithNum((unsigned long long int) i);
	setOrder(e.get_ord());
}

void Zr::init() {
	bn_new(ord);
	//g1_get_ord(ord);
	bn_new(g);
}

//copied element g to the order
void Zr::setOrder(const Zr &order) {
	order.getElement(ord);
}

//Basic initialization to zero
Zr::Zr() {
	init();
	unsigned char z[1];
	z[0] = 0;
	bn_read_bin(g, z, 1);
}

Zr::Zr(const Pairing &e) {
	init();
	setOrder(e.get_ord());
	unsigned char z[1];
	z[0] = 0;
	bn_read_bin(g, z, 1);
}


//Copy constructor
Zr::Zr(const Zr& other) {
	init();
	*this = other;
}

Zr Zr::randomize() {
	bn_rand(g, RLC_POS, bn_bits(ord));
	bn_mod(g, g, ord);
	Zr o(0);
	o.setElement(g);
	o.setOrder(ord);
	return o;
}

//Initialize an element to a specific value
void Zr::initWithNum(unsigned long long int i) {
	init();
	unsigned char chars[8];
	for (int v = 0; v < 8; v++)
		chars[7 - v] = ((i >> (v * 8)) % 256);
	bn_read_bin(g, chars, 8);
}

//Create from hash
Zr::Zr(const Pairing &e, const void *data, unsigned short len) {
	init();
	setOrder(e.get_ord());
	unsigned char o[32];
	int counter = 0;
	for (int i = 0; i < 32; i++) {
		memcpy(o + i, data, min((int) len, 32 - i));
		i += len;
		if (i < 32) {
			o[i] = counter;
			counter++;
		}
	}
	bn_read_bin(g, o, 32);
}

//Init from string
Zr::Zr(const string &s, unsigned short base) {
	init();
	fromString(s, base);
}

//PBC-style import
Zr::Zr(const Pairing &e, const unsigned char *data,
         unsigned short len, unsigned short base) {
    init();
    setOrder(e.get_ord());
    char signed_data[len];
    for (int i = 0; i < len; i++) signed_data[i] = data[i];
    string s(signed_data,len);
    fromString(s,base ? base : 256);
}

//Arithmetic operators
Zr& Zr::operator+=(const Zr& other) { 
	WITH_TEMP(bn,temp, {
		other.getElement(temp);
		bn_add(g,g,temp);
		bn_mod(g,g,ord);
	})
	return *this;
}

Zr& Zr::operator*=(const Zr& other) { 
	WITH_TEMP(bn,temp, {
		other.getElement(temp);
		bn_mul_karat(g,g,temp);
		bn_mod(g,g,ord);
	})
	return *this;
}

Zr& Zr::operator-=(const Zr& other) { 
	WITH_TEMP(bn,temp, {
		other.getElement(temp);
		bn_sub(g,g,temp);
		bn_add(g,g,ord);
		bn_mod(g,g,ord);
	});
	return *this;
}

Zr& Zr::operator/=(const Zr& other) {
	WITH_TEMP(bn,temp, {
		other.getElement(temp);
		WITH_TEMP(bn, inv, {
			WITH_TEMP(bn, trash, {
				bn_gcd_ext(trash,inv,trash,temp,ord); // 1 = gcd = a * other + b * ord, a->inv ie. inv = 1/a mod ord
			});
			bn_add(inv,inv,ord);
			bn_mod(inv,inv,ord);
			bn_mul_comba(g,g,inv);
		});
		bn_mod(g,g,ord);
	});
	return *this;
}

Zr& Zr::operator%=(const Zr& other) {
	WITH_TEMP(bn,temp, {
		other.getElement(temp);
		bn_mod(g,g,temp);
	})
	return *this;
}

Zr& Zr::mod2m_(int m){
	bn_mod_2b(g,g,m);
	return *this;
}

int Zr::get_bit(int pos) const{
	return bn_get_bit(g, pos);
}

Zr& Zr::div_(const Zr& other) {
	WITH_TEMP(bn,temp, {
		other.getElement(temp);
		bn_div(g,g,temp);
	})
	return *this;
}


Zr& Zr::operator^=(const Zr& other) {
	WITH_TEMP(bn,temp, {
		other.getElement(temp);
		bn_mxp(g,g,temp,ord);
	})
	return *this;
}

int Zr::cmp(const Zr& other) const{
	WITH_TEMP_RETURN(bn,temp, {
		other.getElement(temp);
		abs = bn_cmp(g,temp);
	},int,abs)
}

bool Zr::operator==(const Zr& other) const{
	return cmp(other)==0;
}

//Assignment operator
void Zr::operator=(const Zr& other) {
	WITH_TEMP(bn,temp, {
		other.getElement(temp);
		setElement(temp);
	});
	copyOrder(other);
}

//Copy order from another Zr.
void Zr::copyOrder(const Zr& other){
	WITH_TEMP(bn,temp2, {
		other.getOrder(temp2);
		setOrder(temp2);
	});
}

//Outputs to string, readable with fromString
string Zr::toString (unsigned short base) const {
	if (base < 256) {
		char numbs[300];
		bn_write_str(numbs,300,const_cast<bn_st*>(g),(int)base);
		return (base == 10) ? string(numbs) : "\""+string(numbs)+"\"";
	} else {
		char o[32];
		toBin(o);
		return string(o,32);
	}
}

void Zr::toBin(char* output) const {
	bn_write_bin((unsigned char*) output,32,const_cast<bn_st*>(g));
}

void Zr::fromBin(const char* input) {
	bn_read_bin(g,(unsigned char*)const_cast<char*>(input),32);
}

//Used to implement cout << Zr and fromString
void Zr::fromStream(istream &in, int base) {
    if (base == 256) {
      char o[32];
      for (int i = 0; i < 32; i++) { o[i] = (char)in.get(); }
      fromBin(o);
    } else {
      string o;
      in >> o;
      char numbs[300];
      strcpy(numbs,o.c_str());
      //bn_read_str(g, numbs,300,(int)base);
      bn_read_str(g, numbs,(int)o.size(),(int)base);
    }
    bn_mod(g,g,ord);
} 
